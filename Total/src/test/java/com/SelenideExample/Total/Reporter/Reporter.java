package com.SelenideExample.Total.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public abstract class Reporter {

    public static ExtentReports reports = new ExtentReports();
    public static ExtentSparkReporter reporter;
    private static ExtentTest test;

    public static void createReports() {
        if (reporter == null) {
            reporter = new ExtentSparkReporter("./src/test/java/com/SelenideExample/Total/Reports/report.html");
            reports.attachReporter(reporter);
        }
    }

    public static ExtentTest createTest(String testName) {
        test = reports.createTest(testName);
        return test;
    }

    public static ExtentTest test() {
        return test;
    }
}
