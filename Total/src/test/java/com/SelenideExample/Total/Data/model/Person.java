package com.SelenideExample.Total.Data.model;

public class Person {

    private String login;
    private String password;
    private String name;
    private String secondName;
    private String phone;
    private String loginColumbia;
    private String passwordColumbia;


    public String getLogin() {return login;}

    public String getPassword() {return password;}

    public String getName() {return name;}

    public String getSecondName() {return secondName;}

    public String getphone() {return phone;}

    public String getLoginColumbia() {return loginColumbia;}

    public String getPasswordColumbia() {return passwordColumbia;}

}
