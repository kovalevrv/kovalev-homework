package com.SelenideExample.Total.Tests;

import com.SelenideExample.Total.Data.model.Person;
import com.SelenideExample.Total.Helper.TestHelper;
import com.SelenideExample.Total.Pages.PageFactory.Citilink;
import com.SelenideExample.Total.Pages.ScreenShot;
import com.SelenideExample.Total.Pages.StartPage;
import com.SelenideExample.Total.Reporter.Reporter;
import com.aventstack.extentreports.ExtentTest;
import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class Total extends TestHelper {

    ScreenShot screenShot = new ScreenShot();
    Person user = getTestData("person", Person.class);


    Total() throws IOException {
    }

    @Test
    void citiLinkFilter() {
        driver.quit();
        Reporter.createTest("citiLinkFilter");
        open(Configuration.baseUrl);
        $(By.xpath("//span[@class='Button__text jsButton__text'][contains(text(),'Каталог товаров')]")).click();
        $(By.xpath("//a[@class='CatalogMenu__subcategory-link'][contains(text(),'Премиум смартфоны')]")).click();
        $(By.xpath("//span[@class='css-1sylyko erq45vj0'][contains(text(),'Доступен самовывоз')]")).scrollIntoView(true);
        $(By.xpath("//span[@class='css-1sylyko erq45vj0'][contains(text(),'APPLE')]")).click();
        $(By.xpath("//a[@class='js--MainMenu MainMenu__item  Link js--Link Link_type_default'][@href='https://www.citilink.ru/about/delivery/']")).scrollIntoView(true);
        $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='e10fhmjh0 css-18h0chl e1rnnvis0'][@name='input-min'][@data-meta-name='FilterPriceGroup__input-min']")).clear();
        $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='e10fhmjh0 css-18h0chl e1rnnvis0'][@name='input-min'][@data-meta-name='FilterPriceGroup__input-min']")).sendKeys("50000");
        $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='epja35w0 css-1hvig8d e1rnnvis0'][@name='input-max'][@data-meta-name='FilterPriceGroup__input-max']")).clear();
        $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='epja35w0 css-1hvig8d e1rnnvis0'][@name='input-max'][@data-meta-name='FilterPriceGroup__input-max']")).sendKeys("100000");
        String apple = $(By.xpath("//a[@class='ProductCardHorizontal__title  Link js--Link Link_type_default']")).getText();
        assertThat(apple, containsString("Apple"));
    }

    @ParameterizedTest
    @CsvSource({"600, 600", "1280, 740"})
    void diffWindowsSizeTesting(Integer width, Integer height) {
        Reporter.createTest("diffWindowsSizeTesting" + width + "x" + height);
        driver.get(basePath);
        driver.manage().window().setSize(new org.openqa.selenium.Dimension(width, height));
        WebElement element = driver.findElement(By.xpath("//button[@class='Footer__contact-button js--Footer__feedback-trigger Button  jsButton Button_theme_secondary-ghost Button_size_s']"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        screenShot.screenShot("beforeScroll" + width + "x" + height);
        js.executeScript("arguments[0].scrollIntoView(); \n window.scrollBy(0, 50) ", element);
        screenShot.screenShot("afterScroll" + width + "x" + height);
    }

    @Test
    void loginJsonCitilink() {
        driver.quit();
        Reporter.createTest("loginJsonCitilink");
        open(Configuration.baseUrl);
        $(By.xpath("//div[@class='IconAndTextWithCount__text_mainHeader IconAndTextWithCount__text'][contains(text(),'Войти')]")).click();
        $(By.xpath("//input[@class=' InputBox__input js--InputBox__input  js--SignIn__login__container-input']")).sendKeys(user.getLogin());
        $(By.xpath("//input[@class=' InputBox__input js--InputBox__input  js--SignIn__password js--InputPassword InputPassword__container-input']")).sendKeys(user.getPassword());
        Wait().until(ExpectedConditions.elementToBeClickable((By.xpath("//button[@class='SignIn__button js--SignIn__action_sign-in  Button  jsButton Button_theme_primary Button_size_m Button_full-width']")))).click();
        File screenLogin = $(By.xpath("//div[@class='HeaderUserName__name']")).getScreenshotAs(OutputType.FILE);
        File targetLogin = new File("./src/test/java/com/SelenideExample/Total/Screen/loginJsonCitilink.png");
        String name = $(By.xpath("//div[@class='HeaderUserName__name']")).getText();
        screenLogin.renameTo(targetLogin);
        Assertions.assertEquals(name, "Роман");
    }

    @Test
    void loginCitilinkJS() {
        driver.get("https://citilink.ru/");
        Reporter.createTest("loginCitilinkJS");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        driver.findElement(By.xpath("//div[@class='IconAndTextWithCount__text_mainHeader IconAndTextWithCount__text'][contains(text(),'Войти')]")).click();
        WebElement login = driver.findElement(By.xpath("//input[@class=' InputBox__input js--InputBox__input  js--SignIn__login__container-input']"));
        js.executeScript("arguments[0].value='kovalevrv@gmail.com'", login);
        WebElement password = driver.findElement(By.xpath("//input[@class=' InputBox__input js--InputBox__input  js--SignIn__password js--InputPassword InputPassword__container-input']"));
        password.click();
        js.executeScript("arguments[0].value='89119616625'", password);
        driver.findElement(By.xpath("//button[@class='InputPassword__button']")).click();
        WebElement logIn = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@class='SignIn__button js--SignIn__action_sign-in  Button  jsButton Button_theme_primary Button_size_m Button_full-width']")));
        logIn.click();
        String name = driver.findElement(By.xpath("//div[@class='HeaderUserName__name']")).getText();
        File screenLogin = driver.findElement(By.xpath("//div[@class='HeaderUserName__name']")).getScreenshotAs(OutputType.FILE);
        File targetLogin = new File("./src/test/java/com/SelenideExample/Total/Screen/loginCitilinkJS.png");
        screenLogin.renameTo(targetLogin);
        Assertions.assertEquals(name, "Роман");
    }

    @Test
    void buyPageObject() {
        new StartPage()
                .open(basePath)
                .findElement("apple")
                .setFirstName(user.getName())
                //.setFirstName("Roman")
                .setSecondName(user.getSecondName())
                //.setSecondName("Kovalev")
                .setPhone(user.getphone());
        //.setPhone("9119616625");
        ts = (TakesScreenshot) TestHelper.driver;
        ExtentTest reportTest = Reporter.createTest("buyPageObjectScreen");
        reportTest.addScreenCaptureFromBase64String(ts.getScreenshotAs(OutputType.BASE64));
    }

    @Test
    void negativeTest() {
        try {
            driver.quit();
            Reporter.createTest("negativeTest");
            open(Configuration.baseUrl);
            $(By.xpath("//span[@class='Button__text jsButton__text'][contains(text(),'Каталог товаров')]")).click();
            $(By.xpath("//a[@class='CatalogMenu__subcategory-link'][contains(text(),'Премиум смартфоны')]")).click();
            $(By.xpath("//span[@class='css-1sylyko erq45vj0'][contains(text(),'Доступен самовывоз')]")).scrollIntoView(true);
            $(By.xpath("//span[@class='css-1sylyko erq45vj0'][contains(text(),'APPLE')]")).click();
            $(By.xpath("//a[@class='js--MainMenu MainMenu__item  Link js--Link Link_type_default'][@href='https://www.citilink.ru/about/delivery/']")).scrollIntoView(true);
            $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='e10fhmjh0 css-18h0chl e1rnnvis0'][@name='input-min'][@data-meta-name='FilterPriceGroup__input-min']")).clear();
            $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='e10fhmjh0 css-18h0chl e1rnnvis0'][@name='input-min'][@data-meta-name='FilterPriceGroup__input-min']")).sendKeys("50000");
            $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='epja35w0 css-1hvig8d e1rnnvis0'][@name='input-max'][@data-meta-name='FilterPriceGroup__input-max']")).clear();
            $(By.xpath("//div[@class='e1514ezh0 css-1tn5u6r elalcrq0']//input[@class='epja35w0 css-1hvig8d e1rnnvis0'][@name='input-max'][@data-meta-name='FilterPriceGroup__input-max']")).sendKeys("100000");
            String apple = $(By.xpath("//a[@class='ProductCardHorizontal__title  Link js--Link Link_type_default']")).getText();
            assertThat(apple, containsString("Samsung"));
        } catch (AssertionError e) {
            Reporter.test().info("Samsung not found");
        }
    }

    @Test
    public void buyPageFactory() throws InterruptedException {
        try {
            Reporter.createTest("buyPageFactory");
            Citilink citilink = PageFactory.initElements(driver, Citilink.class);
            citilink.open();
            citilink.catalog();
            citilink.allPhones();
            citilink.setPhone("Xiaomi");
            screenShot.screenShot(citilink.nameBuyPhone);
        } catch (NoSuchElementException exception) {
            Reporter.test().info("Phone not found");
        }
    }
}
