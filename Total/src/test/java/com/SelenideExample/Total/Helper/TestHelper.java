package com.SelenideExample.Total.Helper;

import com.SelenideExample.Total.Reporter.Reporter;
import com.codeborne.selenide.Configuration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Map;

public class TestHelper {

    public static WebDriver driver;
    public TakesScreenshot ts;
    public String basePath;
    private static ObjectMapper mapper = new ObjectMapper();
    public static Map<String, Object> data;

    @BeforeAll
    public static void BeforeAll() {
        Configuration.browserPosition = "0x0";
        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
        Configuration.pageLoadTimeout = 10000;
        Configuration.timeout = 15000;
        Configuration.baseUrl = "https://www.citilink.ru/";
        Configuration.reportsFolder = "./src/test/java/com/SelenideExample/Total/Screen/";
        Reporter.createReports();
    }

    @BeforeEach
    public void setProperty() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        data = mapper.readValue(
                new File("./src/test/java/com/SelenideExample/Total/config/baseChrome.json"),
                Map.class);
        System.setProperty("webdriver.chrome.driver", ((String) data.get("chromeDriver")));
        buildDriver();
    }

    @AfterEach
    public void endTest() {
        Reporter.test().pass("Complete");
        Reporter.reports.flush();
        driver.quit();
    }

    public static <T> T getTestData(String dataName, Class<T> className) throws IOException {
        return mapper.readValue(
                new File("./src/test/java/com/SelenideExample/Total/Data/" +
                        dataName + ".json"), className);
    }

    public WebElement getElementByXpath(String xpath) {
        return driver.findElement(By.xpath(xpath));
    }

    public void buildDriver() {
        basePath = ((String) data.get("basePath"));
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds((int) data.get("wait")));
        driver.manage().window().maximize();
    }
}
