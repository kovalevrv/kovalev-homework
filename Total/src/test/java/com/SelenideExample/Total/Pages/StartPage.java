package com.SelenideExample.Total.Pages;

import com.SelenideExample.Total.Helper.TestHelper;
import com.SelenideExample.Total.Pages.PageFactory.StartPageFactory;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.PageFactory;

public class StartPage extends TestHelper {

    public StartPage open(String path) {
        driver.get(path);
        driver.manage().window().maximize();
        return this;
    }

    public BuyData findElement(String word){
        StartPageFactory startPageFactory = PageFactory.initElements(driver, StartPageFactory.class);
        startPageFactory.search(word);
        startPageFactory.searchClick();
        startPageFactory.iphone();
        startPageFactory.basket();
        startPageFactory.openBasket();
        startPageFactory.chekOut();
        return new BuyData().open();
    }
}
