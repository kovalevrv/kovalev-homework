package com.SelenideExample.Total.Pages;

import com.SelenideExample.Total.Helper.TestHelper;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;

public class ScreenShot extends TestHelper {

    public void screenShot (String nameScreen){

        ts = (TakesScreenshot) TestHelper.driver;
        File screen = ts.getScreenshotAs(OutputType.FILE);
        File target = new File("./src/test/java/com/SelenideExample/Total/Screen/"+nameScreen+".png");
        screen.renameTo(target);
    }
}
