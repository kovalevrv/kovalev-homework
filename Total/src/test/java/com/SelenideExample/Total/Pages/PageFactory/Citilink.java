package com.SelenideExample.Total.Pages.PageFactory;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Citilink {

    WebDriver driver;
    public String nameBuyPhone;

    @FindBy(xpath = "//span[@class='Button__text jsButton__text'][contains(text(),'Каталог товаров')]")
    private WebElement catalog;

    @FindBy(xpath = "(//a[@class='CatalogMenu__subcategory-link'][contains(text(),'Смартфоны')])[1]")
    private WebElement allPhones;

    public Citilink(WebDriver driver) {
        this.driver = driver;
    }

    public void open() {
        driver.get("https://www.citilink.ru/");
    }

    public void catalog() {
        catalog.click();
    }

    public void allPhones() {
        allPhones.click();
    }

    public Citilink setPhone(String namePhone) throws InterruptedException {
        WebElement element = driver.findElement(By.xpath("(//a[@class='ProductCardHorizontal__title  Link js--Link Link_type_default'][contains(text(),'" + namePhone + "')])[1]"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,-100)");
        driver.findElement(By.xpath("(//a[@class='ProductCardHorizontal__title  Link js--Link Link_type_default'][contains(text(),'" + namePhone + "')])[1]")).click();
        nameBuyPhone = namePhone;
        return this;
    }


}
