package com.SelenideExample.Total.Pages.PageFactory;

import com.SelenideExample.Total.Helper.TestHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class StartPageFactory extends TestHelper {

    @FindBy(xpath = "//div[@class='MainHeader__search']//input[@class=' InputBox__input js--InputBox__input js--InstantSearch__input  InputSearch__container-input'][@placeholder='Поиск по товарам']")
    private WebElement search;

    @FindBy(xpath = "(//span[@class='InputBox__icon'])[2]")
    private WebElement searchClick;

    @FindBy(xpath = "//a[@class=' ProductCardVertical__name  Link js--Link Link_type_default'][contains(text(),'Apple')]")
    private WebElement iphone;


    @FindBy(xpath = "//div[@class='ProductHeader__buy-button']//span[@class='Button__text jsButton__text'][contains(text(),'В корзину')]")
    private WebElement basket;

    @FindBy(xpath = "//span[@class='Button__text jsButton__text'][contains(text(),'Перейти в корзину')]")
    private WebElement openBasket;

    @FindBy(xpath = "//span[@class='Button__text jsButton__text'][contains(text(),'Перейти к оформлению')]")
    private WebElement chekOut;

    public void search(String word) {
        search.sendKeys(word);
    }

    public void searchClick() {
        searchClick.click();
    }

    public void iphone() {
        iphone.click();
    }

    public void basket() {
        basket.click();
    }

    public void openBasket() {
        openBasket.click();
    }

    public void chekOut() {
        chekOut.click();
    }

}
