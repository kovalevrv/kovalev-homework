package com.SelenideExample.Total.Pages;

import com.SelenideExample.Total.Helper.TestHelper;

public class BuyData extends TestHelper {

    public BuyData open(){
        return this;
    }

    public BuyData setFirstName(String firstName){
        getElementByXpath("//input[@name='firstName']").sendKeys(firstName);
        return this;
    }

    public BuyData setSecondName(String secondName){
        getElementByXpath("//input[@name='lastName']").sendKeys(secondName);
        return this;
    }

    public BuyData setPhone(String phone){
        getElementByXpath("//input[@name='phone']").sendKeys(phone);
        return this;
    }

}

