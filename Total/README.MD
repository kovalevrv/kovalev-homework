UI Testing by Kovalev Roman
=============================
https://gitlab.com/kovalevrv/kovalev-homework/-/tree/TotalKovalev/Total




Описание
------------

Автотесты UI сайта www.citilink.ru
с использованием технологий: 
-Java
-JavaScript
-Junit
-Selenium
-ExtentReports
-Gradle


Структура проекта
------------

      Config/            Конфигурационный файл
      Data/              файл JSON с персональными данными
      Driver/            Chrome драйвер
      Helper/            Класс с основной логикой тестов
      Pages/             PageObject & PafeFactory классы
      Reporter/          Класс отчетности
      Reports/           Файл отчета
      Screen/            Папка скриншотов
      Tests/             Тесты



Минимальные требования
------------
Windows 8.x
Java 1.7
ChromeDriver
Gradle



Запуск тестов
-----------

Тестовый файл находится в директории:
Total/src/test/java/com/SelenideExample/Total/Tests/Total.java



Kovalev Roman
https://gitlab.com/kovalevrv/kovalev-homework
